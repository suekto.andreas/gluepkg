gluepkg
=======
This is a commandline application

AWS Glue Packaging Helper. To prepare packaging according to AWS Glue required 
setup for external libraries dependencies.

This is based on the specification laid down at:
https://docs.aws.amazon.com/glue/latest/dg/aws-glue-programming-python-libraries.html

Install
-------

    ::

        pip install gluepkg

Usage
-----

    ::

        gluepkg --src_dir <src> --pkg_s3_path <target>

+---------------+-----------+-------------------------------------------+
| Arg           | Status    | Description                               |
+===============+===========+===========================================+
| src_dir       | required  | the source directory path to be packaged  |
+---------------+-----------+-------------------------------------------+
| pkg_s3_path   | required  | the target object location in s3 (s3 URI) |
+---------------+-----------+-------------------------------------------+

Basically it creates a zip file unto the desired target location of s3

    example::

        gluepkg --src_dir gluepkg/ --pkg_s3_path s3://bucket/folder/gluepkg.zip

Build
-----
1. Build distribution
    ::

        python setup.py bdist_wheel --universal

2. Distribute to pypi
    ::

        twine upload dist/*

3. project install (development)
    ::
    
        python setup.py install

    afterward you can run the command `gluepkg --src_dir <src> --pkg_s3_path <target>`
    during development of this package

    