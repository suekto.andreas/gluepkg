#!/usr/bin/env bash

set -e

if [ -z "$PYTHON_SOURCE_CODE_DIR" ]; then
    PYTHON_SOURCE_CODE_DIR=$1
    if [ -z "$PYTHON_SOURCE_CODE_DIR" ]; then
        read -p "Enter Python Source Code Dir Path: " PYTHON_SOURCE_CODE_DIR
    fi
    
    if [ -z "$PYTHON_SOURCE_CODE_DIR" ]; then
        echo "ERR: Python Source Code Dir Path need to be provided"
        return
    fi
fi

. venv/bin/activate
. tools/helpers/build-venv.sh
. tools/helpers/build-doc.sh $PYTHON_SOURCE_CODE_DIR