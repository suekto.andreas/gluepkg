#!/usr/bin/env bash

FILE_DIR_TO_TEST=$1

if [ -z "$FILE_DIR_TO_TEST" ]; then 
    echo "ERR: FILE_DIR_TO_TEST arg need to be provided"
    return
fi

if [ -z "$JAVA_HOME" ]; then
    echo "ERR: JAVA_HOME arg need to be provided and needs to be JDK 1.8"
    return
fi

. venv/bin/activate 
cp tools/artifacts/empty-cov.xml .tmp/cov.xml
coverage run --omit="*test*" --source=$FILE_DIR_TO_TEST -m unittest discover -p "*test*.py" -f -s $FILE_DIR_TO_TEST/
coverage xml -o .tmp/cov.xml; coverage report -m