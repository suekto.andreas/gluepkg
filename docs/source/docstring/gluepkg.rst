gluepkg package
===============

Submodules
----------

gluepkg.package module
----------------------

.. automodule:: gluepkg.package
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: gluepkg
    :members:
    :undoc-members:
    :show-inheritance:
