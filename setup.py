"""Setup for Distribution"""
import setuptools


def readme():
    """Loading Readme"""
    with open('README.rst') as filen:
        return filen.read()


setuptools.setup(
    name='gluepkg',
    version='1.0.2',
    entry_points={
        "console_scripts": ['gluepkg = gluepkg.package:main']
    },
    description='Package for AWS Glue Library',
    long_description=readme(),
    author='Andreas',
    author_email='suekto.andreas@gmail.com',
    packages=setuptools.find_packages(),
    install_requires=[
        'boto3',
    ])
